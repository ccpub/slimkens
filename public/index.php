<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../config.php';
/** @var array $config */

// Require autoloader
require __DIR__ . '/../core/autoload.php';

// Require Database
require __DIR__ . '/../core/spot2.php';

// Require Twig
require __DIR__ . '/../core/twig.php';

$globals = [];
$globals['current_language'] = $config['default_language'];

// Start the router preparations
require __DIR__ . '/../core/router-prepare.php';

// Bring in any custom routes.
require __DIR__ . '/../routes.php';

// Run it!
require __DIR__ . '/../core/router-run.php';