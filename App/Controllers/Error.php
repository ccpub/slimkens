<?php

namespace App\Controllers;

class Error extends BaseController {

    public function notFound(): void
    {
        http_response_code(404);
        $this->renderView('fourzerofour');
    }
}