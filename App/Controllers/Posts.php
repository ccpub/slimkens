<?php

namespace App\Controllers;

use App\Models\Post;
use Spot\Mapper;

class Posts extends BaseController
{
    /** @var Mapper */
    private Mapper $postMapper;

    public function __construct()
    {
        parent::__construct();
        $this->postMapper = $this->spot->mapper(Post::class);
    }

    public function master($language): void
    {
        $posts = $this->postMapper->all();

        $this->setTitle('Posts Master');

        $this->renderView('posts-master', [
            'posts' => $posts,
        ]);

    }

    public function detail($language, $id): void
    {
        $post = $this->postMapper->get((int)$id);
        if ($post) {
            $this->setTitle($post->title);
            $this->renderView('post-detail', [
                'post' => $post,
            ]);
            return;
        }
        $this->fourOFour();
    }
}