<?php

namespace App\Controllers;

use Exception;
use JsonException;
use Spot\Locator;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

abstract class BaseController {

    // All controllers start with these.
    protected Environment $twig;
    protected Locator $spot;


    public function __construct() {
        global $spot;
        global $twig;
        $this->spot = $spot;
        $this->twig = $twig;
    }

    protected function renderView(string $view, $data = [], $layout = 'default'): void
    {

        global $config;
        global $globals;

        try {
            $data['config'] = $config;
            $data['globals'] = $globals;

            $content = $this->twig->render('views/' . $view . '.html.twig', $data);

            echo $this->twig->render('layouts/' . $layout . '.html.twig', [
                'config' => $config,
                'globals' => $globals,
                'content' => $content,
            ]);
        } catch (Exception $e) {
            die('Exception in rendering a view: ' . $view . ' :: ' . $e->getMessage() . '<br /><br />' . str_replace("#", "<br />", $e->getTraceAsString()));
        }
    }


    protected function setTitle($title = ''): void
    {
        global $globals;
        $globals['page_title'] = $title;
    }

    protected function renderJson($object): void
    {
        header('Content-Type: application/json');
        try {
            echo json_encode($object, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            die('Exception in rendering the JSON: ' . $e->getTraceAsString());
        }
    }

    protected function fourOFour(): void
    {
        http_response_code(404);
        $this->renderView('fourzerofour');
    }
}