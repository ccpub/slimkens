<?php

namespace App\Controllers;

use App\Models\Post;
use stdClass;

class JSON extends BaseController
{
    /**
     * /json-demo
     */
    public function jsonDemo(): void
    {
        $test       = new stdClass();
        $test->name = "John Smith";
        $test->mail = "smithj@gmail.com";

        $this->renderJson($test);
    }

    public function jsonDemoPosts(): void
    {
        $postMapper = $this->spot->mapper(Post::class);
        $posts = $postMapper->all();
        $this->renderJson($posts);
    }
}