<?php

namespace App\Controllers;

use stdClass;

class Home extends BaseController
{
    public function home(): void
    {
        $this->setTitle('Home');
        $this->renderView('home');
    }
}