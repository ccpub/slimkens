<?php

namespace App\Models;

use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Post extends \Spot\Entity
{
    protected static $table = 'posts';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'title'        => ['type' => 'string', 'required' => true],
            'body'         => ['type' => 'text', 'required' => true],
            'date_created' => ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }

    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [];
    }


    public function permalink(): string
    {
        global $globals;

        return '/' . $globals['current_language'] . '/posts/' . $this->id;
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        $data['permalink'] = $this->permalink();
        return $data;
    }

}