<?php

// Create Router instance
use Bramus\Router\Router;

$router = new Router();


// Set the language if the url is prefixed with a language from config.
$router->before('GET|POST', '/'.$config['language_route_prefix'].'/', function($language) {
    // ... this will always be executed on routes that match the languages in config /en /fr /nl ...
    global $globals;
    $globals['current_language'] = $language;
});
$router->before('GET|POST', '/'.$config['language_route_prefix'].'/.*', function($language) {
    // ... this will always be executed on routes that match the languages in config /en/ /fr/ /nl/ ...
    global $globals;
    $globals['current_language'] = $language;
});

// Set the default controller namespace.
$router->setNamespace('\App\Controllers');

// Set the 404 to the Error controller.
$router->set404('Error@notFound');

$router->get('/', function(){
    global $globals;
    header("Location: /" . $globals['current_language'], true, 301);
    exit(0);
});