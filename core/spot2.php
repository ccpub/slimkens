<?php


use App\Models\Post;
use Spot\Config;
use Spot\Locator;

$cfg = new Config();

// Sqlite
try {
    $cfg->addConnection('mysql', $config['dsn']);
} catch (\Spot\Exception $e) {
    echo $e->getMessage();
    exit();
}

// Sqlite
$spot = new Locator($cfg);

$postMapper = $spot->mapper(Post::class);