<?php

// Require composer autoloader
require __DIR__ . '/../vendor/autoload.php';

// Then make our autloader.
spl_autoload_register(static function ($class_name) {
    $file = __DIR__ . '/../' . str_replace("\\", "/", $class_name) . '.php';
    if (file_exists($file)) {
        require_once($file);
    }
});