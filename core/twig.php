<?php

// Light up the twig.
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

$loader             = new FilesystemLoader(__DIR__ . '/../App/templates');
$twig = new Environment($loader, [
    //'cache' => __DIR__ . '/../compilation_cache',
    'debug' => true,
]);
$twig->addExtension(new DebugExtension());