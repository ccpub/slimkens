<?php

/** @var \Bramus\Router\Router $router */
/** @var array $config */

$l = $config['language_route_prefix'];


$router->get('/' . $l, 'Home@home');
$router->get('/' . $l . '/posts', 'Posts@master');
$router->get('/' . $l . '/posts/{id}', 'Posts@detail');


$router->get('/json-demo', 'JSON@jsonDemo');
$router->get('/json-demo/posts', 'JSON@jsonDemoPosts');





