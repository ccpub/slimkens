<?php
/** @var $config */
$config = [];

/// SITE CONFIG
$config['site_title'] = 'Slimkens MVC';
$config['dsn'] = 'mysql://root:root@127.0.0.1/slimkens';



/// LANGUAGES
$config['languages'] = [];
$config['languages']['en'] = 'English';
$config['languages']['fr'] = 'Français';
$config['languages']['nl'] = 'Nederlands';

$config['default_language'] = 'en';
$config['language_route_prefix'] = '('.implode('|', array_keys($config['languages'])).')';




